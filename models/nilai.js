"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class nilai extends Model {
    static associate(models) {
      models.nilai.belongsTo(models.mahasiswa, {
        foreignKey: "id_mhs",
      }),
        models.nilai.belongsTo(models.matkul, {
          foreignKey: "id_matkul",
        });
    }
  }
  nilai.init(
    {
      id_matkul: DataTypes.INTEGER,
      id_mhs: DataTypes.INTEGER,
      nilai: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "nilai",
    }
  );
  return nilai;
};
