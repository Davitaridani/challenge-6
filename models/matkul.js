"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class matkul extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.matkul.hasOne(models.nilai, {
        foreignKey: "id_mhs",
      });
    }
  }
  matkul.init(
    {
      kd_matkul: DataTypes.INTEGER,
      nama_matkul: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "matkul",
    }
  );
  return matkul;
};
