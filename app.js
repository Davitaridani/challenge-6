const express = require("express");
const app = express();
const port = 3002;
const mahasiswaData = require("./router/mahasiswaRouter");
const matkulData = require("./router/matkulRouter");
const nilaiData = require("./router/nilaiRouter");
const loginController = require("./controllers/loginController");

app.use(express.json());
app.use(express.urlencoded());

app.use(express.static("public"));
app.set("view engine", "ejs");

app.use("/", mahasiswaData);
app.use("/", matkulData);
app.use("/", nilaiData);
app.use(loginController);

app.get("/", async (req, res) => {
  res.redirect("/login");
});

app.get("/dashboard", (req, res) => {
  res.render("dashboard");
});

app.listen(port, () => {
  console.log("server running on port  " + port);
});
