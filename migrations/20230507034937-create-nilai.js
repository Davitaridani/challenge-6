"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("nilais", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      id_matkul: {
        type: Sequelize.INTEGER,
        references: {
          model: "matkuls",
          key: "id",
        },
      },
      id_mhs: {
        type: Sequelize.INTEGER,
        references: {
          model: "mahasiswas",
          key: "id",
        },
      },
      nilai: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("nilais");
  },
};
