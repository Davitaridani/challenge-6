const express = require("express");
const router = express.Router();
const { nilai, matkul, mahasiswa } = require("../models");

router.get("/nilai", async (req, res) => {
  const nilais = await nilai.findAll({
    include: [matkul, mahasiswa],
  });
  res.render("nilai", { nilais });
});

router.get("/add-nilai", async (req, res) => {
  const nilais = await nilai.findAll({
    include: [matkul, mahasiswa],
  });
  res.render("addNilai", { nilais });
});

router.post("/nilai", async (req, res) => {
  if (req.body._method) {
    await nilai.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await nilai.create(req.body);
  }
  res.redirect("/nilai");
});

router.get("/edit-nilai/:id", async (req, res) => {
  const nilais = await nilai.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("editNilai", { nilais });
});

router.post("/delete-nilai", async (req, res) => {
  if (req.body._method === "DELETE") {
    await nilai.destroy({
      where: {
        id: req.body.id,
      },
    });
  }
  res.redirect("/nilai");
});

module.exports = router;
