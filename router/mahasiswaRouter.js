const express = require("express");
const router = express.Router();
const { mahasiswa } = require("../models");

router.get("/mahasiswa", async (req, res) => {
  const mahasiswas = await mahasiswa.findAll({});
  res.render("mahasiswa", { mahasiswas });
});

router.get("/add-mahasiswa", async (req, res) => {
  const mahasiswas = await mahasiswa.findAll({});
  res.render("addmahasiswa", { mahasiswas });
});

router.post("/mahasiswa", async (req, res) => {
  if (req.body._method) {
    await mahasiswa.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await mahasiswa.create(req.body);
  }
  res.redirect("/mahasiswa");
});

// EDIT mahasiswa
router.get("/edit-mahasiswa/:id", async (req, res) => {
  const mahasiswas = await mahasiswa.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("editMahasiswa", { mahasiswas });
});

// DELETE mahasiswa
router.post("/delete-mahasiswa", async (req, res) => {
  if (req.body._method === "DELETE") {
    await mahasiswa.destroy({
      where: {
        id: req.body.id,
      },
    });
  }
  res.redirect("/mahasiswa");
});

module.exports = router;
