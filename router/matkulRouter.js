const express = require("express");
const router = express.Router();
const { matkul } = require("../models");

router.get("/matkul", async (req, res) => {
  const matkuls = await matkul.findAll({});
  res.render("matkul", { matkuls });
});

router.get("/add-matkul", async (req, res) => {
  const matkuls = await matkul.findAll({});
  res.render("addMatkul", { matkuls });
});

router.post("/matkul", async (req, res) => {
  if (req.body._method) {
    await matkul.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await matkul.create(req.body);
  }
  res.redirect("/matkul");
});

// EDIT matkul
router.get("/edit-matkul/:id", async (req, res) => {
  const matkuls = await matkul.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("editMatkul", { matkuls });
});

// DELETE matkul
router.post("/delete-matkul", async (req, res) => {
  if (req.body._method === "DELETE") {
    await matkul.destroy({
      where: {
        id: req.body.id,
      },
    });
  }
  res.redirect("/matkul");
});

module.exports = router;
